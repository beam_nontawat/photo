package app.domain.repository

import app.common.base_result.BaseResult
import app.domain.enities.photo.PhotoResponseModel
import kotlinx.coroutines.flow.Flow

interface PhotoRepository{

     fun getPhotoList(): Flow<BaseResult<List<PhotoResponseModel>>>

}