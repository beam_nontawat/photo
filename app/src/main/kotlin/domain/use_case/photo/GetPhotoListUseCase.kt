package app.domain.use_case.photo

import app.common.base_class.BaseUseCase
import app.common.base_class.NoParams
import app.common.base_result.BaseResult
import app.domain.enities.photo.PhotoResponseModel
import app.domain.repository.PhotoRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPhotoListUseCase @Inject constructor(
   private val photoRepository: PhotoRepository
) : BaseUseCase<NoParams, List<PhotoResponseModel>>() {

    override  fun invoke(input: NoParams): Flow<BaseResult<List<PhotoResponseModel>>> {
        return  photoRepository.getPhotoList()
    }

}