package app.common.base_data_source

import app.common.base_result.BaseResult
import retrofit2.Response

abstract class BaseDataSource {
    suspend inline fun <reified T : Any> remoteRunCatching(
        crossinline action: suspend () -> Response<T>
    ): BaseResult<T> {
        return try {
            val response = action()
            if (response.isSuccessful && response.body() != null) {
                BaseResult.Success(response.body())
            } else {
                @Suppress("BlockingMethodInNonBlockingContext")
                BaseResult.Error(
                    message = response.message(),
                    errorBody = response.errorBody()?.string(),
                    code = response.code()
                )
            }
        } catch (e: Exception) {
            BaseResult.Error(
                message = e.message,
                errorBody = null,
                code = null
            )
        }
    }

}
