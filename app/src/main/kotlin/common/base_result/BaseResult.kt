package app.common.base_result

sealed class BaseResult<out T : Any> {
    data class Success<T : Any>(val data: T?) : BaseResult<T>()
    data class Error(val message: String?, val code: Int?, val errorBody: String?) : BaseResult<Nothing>()
    object NoInternet : BaseResult<Nothing>()
    object Loading : BaseResult<Nothing>()
}