package app.common.base_class

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.annotation.ColorRes
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import app.BR


abstract class BaseFragment<T : ViewDataBinding, VM : ViewModel>(
    @LayoutRes layoutIn: Int
) : Fragment() {

    private lateinit var _binding: T
    private val layout = layoutIn

    protected val binding get() = _binding
    protected open val navController by lazy { findNavController() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initial()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            onBackPressed()
        }
        _binding = DataBindingUtil.inflate(layoutInflater, layout, container, false)
        _binding.lifecycleOwner = viewLifecycleOwner
        _binding.setVariable(BR._all, vm)
        _binding.executePendingBindings()
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
        updateUI(view, savedInstanceState)
        listenerUI()
    }

    protected fun setLightStatusBars(isLightStatusBars: Boolean) {
        val controllerCompat = WindowInsetsControllerCompat(requireActivity().window, binding.root)
        controllerCompat.isAppearanceLightStatusBars = !isLightStatusBars
    }

    protected fun setColorBars(@ColorRes color: Int) {
        requireActivity().window.statusBarColor = ContextCompat
            .getColor(requireContext(), color)
    }

    protected open fun onBackPressed() {
        if (!navController.popBackStack()) requireActivity().finish()
    }

    protected fun <T> navControllerGetBackArgument(key: String): MutableLiveData<T>? {
        val savedStateHandle = navController.currentBackStackEntry?.savedStateHandle
        val dataHolder = savedStateHandle?.getLiveData<T>(key)
        savedStateHandle?.remove<T>(key)
        return dataHolder
    }

    protected fun <T> navControllerSetBackArgument(key: String, data: T) {
        navController.previousBackStackEntry?.savedStateHandle?.set(key, data)
        navController.popBackStack()
    }

    protected fun <T> navControllerSetBackArgument(
        destinationId: Int,
        inclusive: Boolean,
        key: String,
        data: T,
    ) {
        navController.getBackStackEntry(destinationId).savedStateHandle[key] = data
        navController.popBackStack(destinationId, inclusive)
    }

    protected abstract val vm: VM
    abstract fun initial()
    abstract fun updateUI(view: View, savedInstanceState: Bundle?)
    abstract fun listenerUI()
    abstract fun observer()
}