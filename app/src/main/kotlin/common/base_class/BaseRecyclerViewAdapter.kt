package app.common.base_class

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerViewAdapter<RV : RecyclerView.ViewHolder,T> :
    RecyclerView.Adapter<RV >()  {

    abstract val mData : MutableList<T>
    abstract fun setDataItem(data : List<T>)

    protected fun getDiffResult(
        oldList: List<T>,
        newList: List<T>,
        itemsTheSame: (oldItem: T, newItem: T) -> Boolean
    ): DiffUtil.DiffResult {
        return DiffUtil.calculateDiff(BaseDiffUtil(oldList, newList, itemsTheSame))
    }

    protected fun setDiffResult(diffResult: DiffUtil.DiffResult) {
        diffResult.dispatchUpdatesTo(this)
    }

    inner class BaseDiffUtil(
        private val oldList: List<T>,
        private val newList: List<T>,
        private val itemsTheSame: (oldItem: T, newItem: T) -> Boolean
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldList[oldItemPosition]
            val newItem = newList[newItemPosition]
            return itemsTheSame(oldItem, newItem)
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]
    }

}