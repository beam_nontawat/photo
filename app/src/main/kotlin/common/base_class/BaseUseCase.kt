package app.common.base_class

import app.common.base_result.BaseResult
import kotlinx.coroutines.flow.Flow

abstract  class BaseUseCase<IN : BaseParams, OUT : Any> {

    abstract  operator fun invoke(input: IN): Flow<BaseResult<OUT>>

}

interface BaseParams

class NoParams : BaseParams

data  class Params<T>(val model : T) : BaseParams