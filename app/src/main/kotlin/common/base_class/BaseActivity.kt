package app.common.base_class

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel

abstract class BaseActivity<T: ViewDataBinding, VM: ViewModel>(
    @LayoutRes layoutIn : Int
)  : AppCompatActivity(){

    private lateinit var _binding: T

    private val layout = layoutIn
    protected val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = DataBindingUtil.setContentView(this,layout)
        initial()
        updateUI(savedInstanceState)
        listenerUI()
        observer()
    }

    protected fun setStatusBarTransparentFull() {
        WindowCompat.setDecorFitsSystemWindows(window, false)
    }

    protected abstract val vm : VM
    abstract fun initial()
    abstract fun updateUI(savedInstanceState: Bundle?)
    abstract fun listenerUI()
    abstract fun observer()
}