package app.common.condition_result

import app.common.base_result.BaseResult

/**
 * run execute
 * */
suspend inline fun <T : Any> onExecute(
    crossinline onExecute: suspend () -> BaseResult<T>
): BaseResult<T> {
    return onExecute()
}

/**
 * check Internet connection
 * */
suspend inline fun <T : Any> onCheckInternet(
    isInternetConnect: Boolean,
    crossinline onCheckInternet: suspend () -> BaseResult<T>
): BaseResult<T> {
    return when (isInternetConnect) {
        true -> {
            onCheckInternet()
        }
        else -> {
            BaseResult.NoInternet
        }
    }
}

/**
 * check is BaseResult.Success
 * */
suspend inline fun <T : Any> BaseResult<T>.onSuccess(
    crossinline onSuccess: suspend T.() -> Unit
): BaseResult<T> {
    if (this is BaseResult.Success) {
        onSuccess(this.data!!)
    }
    return this
}

/**
 * check is BaseResult.NoInternet
 * */
suspend inline fun <T : Any> BaseResult<T>.onNoInternet(
    crossinline onNoInternet: suspend () -> Unit
): BaseResult<T> {
    if (this is BaseResult.NoInternet) {
        onNoInternet()
    }
    return this
}

/**
 * check is BaseResult.Error
 * */
suspend inline fun <T : Any> BaseResult<T>.onError(
    crossinline onError: suspend BaseResult.Error.() -> Unit
): BaseResult<T> {
    if (this is BaseResult.Error) {
        onError(this)
    }
    return this
}