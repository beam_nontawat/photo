package app.presentation.photo_view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import app.R
import app.common.base_class.BaseFragment
import app.databinding.FragmentPhotoViewBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PhotoViewFragment : BaseFragment<FragmentPhotoViewBinding, PhotoViewViewModel>(
    R.layout.fragment_photo_view
) {

    private val args: PhotoViewFragmentArgs by navArgs()

    override val vm: PhotoViewViewModel by viewModels()

    override fun initial() {
    }

    override fun updateUI(view: View, savedInstanceState: Bundle?) {
        setColorBars(android.R.color.black)
        binding.tvToolbar.text = args.model?.title.orEmpty()
        Glide.with(requireContext())
            .load(GlideUrl(
                args.model?.url.orEmpty(),
                LazyHeaders.Builder().addHeader("User-Agent", "your-user-agent").build()
            ))
            .placeholder(android.R.color.darker_gray)
            .into(binding.imvPhoto)
    }

    override fun listenerUI() {
        binding.btnBack.setOnClickListener {
            onBackPressed()
        }
    }


    override fun observer() {
    }


}