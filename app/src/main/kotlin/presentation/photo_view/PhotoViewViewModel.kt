package app.presentation.photo_view

import androidx.lifecycle.SavedStateHandle
import app.common.base_class.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PhotoViewViewModel @Inject constructor(
    val state: SavedStateHandle,
) : BaseViewModel() {
}