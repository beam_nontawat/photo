package app.presentation

import android.os.Bundle
import androidx.activity.viewModels
import app.R
import app.common.base_class.BaseActivity
import app.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity
    : BaseActivity<ActivityMainBinding, MainViewModel>(
    R.layout.activity_main
) {
    override val vm: MainViewModel by viewModels()

    override fun initial() {
    }

    override fun updateUI(savedInstanceState: Bundle?) {
       checkNavigation()
    }

    override fun listenerUI() {

    }

    override fun observer() {
    }


    private fun checkNavigation() {

    }


}