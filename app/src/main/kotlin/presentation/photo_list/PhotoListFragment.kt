package app.presentation.photo_list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import app.R
import app.common.base_class.BaseFragment
import app.common.base_result.BaseResult
import app.databinding.FragmentPhotoListBinding
import app.presentation.photo_list.adapter.PhotoAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PhotoListFragment : BaseFragment<FragmentPhotoListBinding, PhotoListViewModel>(
    R.layout.fragment_photo_list
) {

    private val photoAdapter : PhotoAdapter by lazy {
        PhotoAdapter()
    }
    override val vm: PhotoListViewModel by viewModels()

    override fun initial() {
    }

    override fun updateUI(view: View, savedInstanceState: Bundle?) {
        setColorBars(android.R.color.black)
        setAdapter()
    }

    override fun listenerUI() {
        photoAdapter.onClickMenu = {
            kotlin.runCatching {
                navController.navigate(
                    PhotoListFragmentDirections.actionPhotoListFragmentToPhotoViewFragment(it)
                )
            }
        }
        binding.swipeRefresh.setOnRefreshListener {
            vm.getPhotoList()
        }
    }

    override fun observer() {
        lifecycleScope.launchWhenStarted {
            vm.photoList.collect {
                when(it) {
                    is BaseResult.Loading -> {
                        binding.swipeRefresh.isRefreshing = true
                    }
                    is BaseResult.Success -> {
                        binding.swipeRefresh.isRefreshing = false
                        photoAdapter.setDataItem(it.data.orEmpty())
                    }
                    is BaseResult.Error -> {
                    }
                    is BaseResult.NoInternet -> {
                    }
                }
            }
        }
    }

    private fun setAdapter() {
        binding.rcvPhoto.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = photoAdapter
        }
    }
}