package app.presentation.photo_list.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import app.R
import app.common.base_class.BaseRecyclerViewAdapter
import app.common.base_class.BaseViewHolder
import app.databinding.ItemPhotoBinding
import app.domain.enities.photo.PhotoResponseModel
import app.utils.setSafeOnClickListener
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders


class PhotoAdapter : BaseRecyclerViewAdapter<BaseViewHolder<*>, PhotoResponseModel>() {

    var onClickMenu: ((navigation: PhotoResponseModel) -> Unit)? = null

    override val mData: MutableList<PhotoResponseModel> = mutableListOf()

    override fun setDataItem(data: List<PhotoResponseModel>) {
        val diff = getDiffResult(this.mData, data) { oldItem, newItem ->
            oldItem.id == newItem.id
        }
        mData.clear()
        mData.addAll(data)
        setDiffResult(diff)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.item_photo,
            parent,
            false
        ) as ItemPhotoBinding
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is ViewHolder -> holder.bind(position)
            else -> throw IllegalAccessException()
        }
    }

    override fun getItemCount(): Int = mData.size


    inner class ViewHolder(binding: ItemPhotoBinding) :
        BaseViewHolder<ItemPhotoBinding>(binding) {

        init {
            binding.imvPhoto.setSafeOnClickListener {
                onClickMenu?.invoke(mData[adapterPosition])
            }
        }

        @SuppressLint("SetTextI18n")
        override fun bind(position: Int) {
            val data = mData[position]
            val context = binding.root.context

            Glide.with(context)
                .load(GlideUrl(
                    data.thumbnailUrl,
                    LazyHeaders.Builder().addHeader("User-Agent", "your-user-agent").build()
                ))
                .placeholder(android.R.color.darker_gray)
                .into(binding.imvPhoto)

            binding.tvName.text = data.title

            binding.spacer.visibility = View.GONE

            if (data == mData.last()){
                binding.spacer.visibility = View.VISIBLE
            }
        }
    }
}
