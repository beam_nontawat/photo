package app.presentation.photo_list

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import app.common.base_class.BaseViewModel
import app.common.base_class.NoParams
import app.common.base_result.BaseResult
import app.domain.enities.photo.PhotoResponseModel
import app.domain.use_case.photo.GetPhotoListUseCase
import app.presentation._di.IoDispatcher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PhotoListViewModel @Inject constructor(
    private val getPhotoListUseCase: GetPhotoListUseCase,
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    val state: SavedStateHandle,
) : BaseViewModel() {

    private val _photoList = MutableStateFlow<BaseResult<List<PhotoResponseModel>>>(BaseResult.Loading)
    val photoList = _photoList.asStateFlow()

    init {
        getPhotoList()
    }

    fun getPhotoList() {
        viewModelScope.launch(dispatcher) {
            _photoList.emit(BaseResult.Loading)
            getPhotoListUseCase(
                NoParams()
            ).collect {
                _photoList.emit(it)
            }
        }
    }
}