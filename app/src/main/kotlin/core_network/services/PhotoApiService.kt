package app.core_network.services

import app.domain.enities.photo.PhotoResponseModel
import retrofit2.Response
import retrofit2.http.*

interface PhotoApiService {

    @GET("/albums/1/photos")
    suspend fun getPhotoList(
    ): Response<List<PhotoResponseModel>>


}
