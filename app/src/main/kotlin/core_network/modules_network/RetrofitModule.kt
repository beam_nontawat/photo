package app.core_network.modules_network

import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit

class RetrofitModule constructor(
    private val okHttpClient: OkHttpClient,
    private val converterFactory: Converter.Factory,
) {
     fun build(baseUrl: String): Retrofit {
        return with(
            Retrofit.Builder()
        ) {
            client(okHttpClient)
            baseUrl(baseUrl)
            addConverterFactory(converterFactory)
            build()
        }
    }
}