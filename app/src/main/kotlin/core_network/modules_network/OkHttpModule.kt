package app.core_network.modules_network

import app.core_network.modules_network.interceptor.DefaultRequestInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

class OkHttpModule constructor(
    private val defaultRequestInterceptor: DefaultRequestInterceptor,
    private val loggingInterceptor: HttpLoggingInterceptor,
) {
    fun build(): OkHttpClient {
        return with(
            OkHttpClient.Builder()
        ) {
            addInterceptor(defaultRequestInterceptor)
            addInterceptor(loggingInterceptor)
            retryOnConnectionFailure(true)
            connectTimeout(35, TimeUnit.SECONDS)
            writeTimeout(35, TimeUnit.SECONDS)
            readTimeout(35, TimeUnit.SECONDS)
            build()
        }
    }
}