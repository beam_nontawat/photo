package app.core_network.modules_network.interceptor

import app.BuildConfig
import com.orhanobut.logger.Logger.i
import com.orhanobut.logger.Logger.json
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONException
import org.json.JSONObject

class OkHttpLoggingInterceptor {
    fun build(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor { message ->
            try {
                JSONObject(message)
                json(message)
            } catch (error: JSONException) {
                i(message)
            }
        }.apply {
            if (BuildConfig.DEBUG) {
                setLevel(HttpLoggingInterceptor.Level.BODY)
            }
        }
    }
}