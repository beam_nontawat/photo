package app.core_network.modules_network.interceptor

import android.annotation.SuppressLint
import okhttp3.Interceptor
import okhttp3.Response

class DefaultRequestInterceptor() : Interceptor {

    @SuppressLint("HardwareIds")
    override fun intercept(chain: Interceptor.Chain): Response {

        return chain.proceed(
            with(
                chain.request().newBuilder()
            ) {
                addHeader("Content-Type", "application/json")
                addHeader("os", "Android")
                build()
            }
        )
    }
}
