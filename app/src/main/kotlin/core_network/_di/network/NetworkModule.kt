package app.core_network._di.network

import app.core_network.modules_network.OkHttpModule
import app.core_network.modules_network.interceptor.DefaultRequestInterceptor
import app.core_network.modules_network.interceptor.OkHttpLoggingInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Singleton
    @Provides
    fun provideGsonConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Singleton
    @Provides
    fun provideDefaultRequestInterceptor(
    ): DefaultRequestInterceptor {
        return DefaultRequestInterceptor()
    }

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return OkHttpLoggingInterceptor().build()
    }

    @Singleton
    @Provides
    fun provideOkHttp(
        defaultRequestInterceptor: DefaultRequestInterceptor,
        httpLoggingInterceptor: HttpLoggingInterceptor,
    ): OkHttpClient {
        return OkHttpModule(
            defaultRequestInterceptor,
            httpLoggingInterceptor,
        ).build()
    }
}

