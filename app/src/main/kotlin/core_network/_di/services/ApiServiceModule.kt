package app.core_network._di.services

import app.BuildConfig
import app.core_network.modules_network.RetrofitModule
import app.core_network.services.PhotoApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Converter
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApiServiceModule {

    @Singleton
    @Provides
    fun providePhotoApiService(
        okHttp: OkHttpClient,
        gsonConverterFactory: Converter.Factory,
    ): PhotoApiService {
        return with(
            RetrofitModule(okHttp, gsonConverterFactory)
        ) {
            build(BuildConfig.API_URL)
        }.create(PhotoApiService::class.java)
    }



}

