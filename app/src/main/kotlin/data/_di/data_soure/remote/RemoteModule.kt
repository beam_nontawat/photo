package app.data._di.data_soure.remote

import app.data.data_sources.remote.PhotoRemote
import app.data.data_sources.remote.PhotoRemoteImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RemoteModule{

    @Singleton
    @Binds
    abstract fun bindsPhotoRemoteImpl(
        photoRemoteImpl: PhotoRemoteImpl,
    ): PhotoRemote

}