package app.data._di.repository

import app.data.repositoriesImpl.PhotoRepositoryImpl
import app.domain.repository.PhotoRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule{

    @Singleton
    @Binds
    abstract fun bindsPhotoRepositoryImpl(
        photoRepositoryImpl: PhotoRepositoryImpl,
    ): PhotoRepository

}
