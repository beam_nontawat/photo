package app.data.repositoriesImpl

import app.common.base_result.BaseResult
import app.common.condition_result.onError
import app.common.condition_result.onExecute
import app.common.condition_result.onSuccess
import app.data.data_sources.remote.PhotoRemote
import app.domain.enities.photo.PhotoResponseModel
import app.domain.repository.PhotoRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class PhotoRepositoryImpl @Inject constructor(
    private val photoRemote: PhotoRemote
) : PhotoRepository {

    override  fun getPhotoList(): Flow<BaseResult<List<PhotoResponseModel>>> {
        return flow {
            onExecute {
                photoRemote.getPhotoList()
            }.onSuccess {
                emit(BaseResult.Success(this))
            }.onError {
                emit(this)
            }
        }
    }
}

