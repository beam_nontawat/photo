package app.data.data_sources.remote

import app.common.base_result.BaseResult
import app.domain.enities.photo.PhotoResponseModel

interface PhotoRemote {

    suspend fun getPhotoList(): BaseResult<List<PhotoResponseModel>>

}