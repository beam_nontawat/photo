package app.data.data_sources.remote

import app.common.base_data_source.BaseDataSource
import app.common.base_result.BaseResult
import app.core_network.services.PhotoApiService
import app.domain.enities.photo.PhotoResponseModel
import javax.inject.Inject

class PhotoRemoteImpl @Inject constructor(
    private val apiService: PhotoApiService
) : BaseDataSource(), PhotoRemote {

    override suspend fun getPhotoList(): BaseResult<List<PhotoResponseModel>> {
        return remoteRunCatching {
            apiService.getPhotoList()
        }
    }
}