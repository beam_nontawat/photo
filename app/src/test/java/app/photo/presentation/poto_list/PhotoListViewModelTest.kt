package app.photo.presentation.poto_list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import app.common.base_class.NoParams
import app.common.base_result.BaseResult
import app.domain.enities.photo.PhotoResponseModel
import app.domain.repository.PhotoRepository
import app.domain.use_case.photo.GetPhotoListUseCase
import app.photo.tools.CoroutineRuleTest
import app.presentation.photo_list.PhotoListViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations


class PhotoListViewModelTest {

    @get:Rule
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineRuleTest: CoroutineRuleTest = CoroutineRuleTest()

    @Mock
    private lateinit var photoRepository: PhotoRepository
    private lateinit var getPhotoListUseCase: GetPhotoListUseCase
    private lateinit var photoListViewModel: PhotoListViewModel

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        getPhotoListUseCase = GetPhotoListUseCase(photoRepository)

    }


    @ExperimentalCoroutinesApi
    @Test
    fun `getPhotoList - Should Success`() = runTest(UnconfinedTestDispatcher()) {
        `when`(getPhotoListUseCase.invoke(NoParams())).thenReturn(
            flow {
                emit(getPhotoListSuccess)
            }
        )
        photoListViewModel = PhotoListViewModel(
            getPhotoListUseCase,
            UnconfinedTestDispatcher(),
            SavedStateHandle(),
        )
        val result = photoListViewModel.photoList.value
        Assert.assertTrue(result is BaseResult.Success)
        Assert.assertEquals(result, getPhotoListSuccess)

        photoListViewModel.getPhotoList()
        Assert.assertTrue(result is BaseResult.Success)
        Assert.assertEquals(result, getPhotoListSuccess)
    }



    @ExperimentalCoroutinesApi
    @Test
    fun `getPhotoList - Should Error`() = runTest(UnconfinedTestDispatcher()) {
        `when`(getPhotoListUseCase.invoke(NoParams())).thenReturn(
            flow {
                emit(getPhotoListError)
            }
        )
        photoListViewModel = PhotoListViewModel(
            getPhotoListUseCase,
            UnconfinedTestDispatcher(),
            SavedStateHandle(),
        )

        val result = photoListViewModel.photoList.value
        Assert.assertTrue(result is BaseResult.Error)
        Assert.assertEquals(result, getPhotoListError)

        photoListViewModel.getPhotoList()
        Assert.assertTrue(result is BaseResult.Error)
        Assert.assertEquals(result, getPhotoListError)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `getPhotoList - Should Loading`() = runTest(UnconfinedTestDispatcher()) {
        `when`(getPhotoListUseCase.invoke(NoParams())).thenReturn(
            flow {
                delay(1000)
                emit(getPhotoListError)
            }
        )

        photoListViewModel = PhotoListViewModel(
            getPhotoListUseCase,
            UnconfinedTestDispatcher(),
            SavedStateHandle(),
        )

        val result = photoListViewModel.photoList.value
        Assert.assertTrue(result is BaseResult.Loading)

        photoListViewModel.getPhotoList()
        Assert.assertTrue(result is BaseResult.Loading)
    }

    companion object {
        val getPhotoListSuccess = BaseResult.Success(
            listOf(
                PhotoResponseModel(
                    albumId = "1",
                    id = "1",
                    title = "title1",
                    url = "url1",
                    thumbnailUrl = "thumbnailUrl1"
                ),
                PhotoResponseModel(
                    albumId = "1",
                    id = "2",
                    title = "title2",
                    url = "url2",
                    thumbnailUrl = "thumbnailUrl2"
                )
            )
        )

        val getPhotoListError = BaseResult.Error(
            message = "message",
            code = 500,
            errorBody = "errorBody"
        )
    }

}