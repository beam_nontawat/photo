package app.photo.domain.photo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.common.base_class.NoParams
import app.common.base_result.BaseResult
import app.domain.enities.photo.PhotoResponseModel
import app.domain.repository.PhotoRepository
import app.domain.use_case.photo.GetPhotoListUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetPhotoListUseCaseTest {


    @get:Rule
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var photoRepository: PhotoRepository
    private lateinit var getPhotoListUseCase: GetPhotoListUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        getPhotoListUseCase = GetPhotoListUseCase(photoRepository)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `getPhotoList - Should Success`() = runTest {
        `when`(photoRepository.getPhotoList()).thenReturn(
            flow {
                emit(getPhotoListSuccess)
            }
        )
        val result = getPhotoListUseCase.invoke(NoParams()).first()
        Assert.assertTrue(result is BaseResult.Success)
        Assert.assertEquals((result as BaseResult.Success).data, getPhotoListSuccess.data)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `getPhotoList - Should Error`() = runTest {
        `when`(photoRepository.getPhotoList()).thenReturn(
            flow {
                emit(getPhotoListError)
            }
        )
        val result = getPhotoListUseCase.invoke(NoParams()).first()
        Assert.assertTrue(result is BaseResult.Error)
        Assert.assertEquals((result as BaseResult.Error), getPhotoListError)
    }

    companion object {
        val getPhotoListSuccess = BaseResult.Success(
            listOf(
                PhotoResponseModel(
                    albumId = "1",
                    id = "1",
                    title = "title1",
                    url = "url1",
                    thumbnailUrl = "thumbnailUrl1"
                ),
                PhotoResponseModel(
                    albumId = "1",
                    id = "2",
                    title = "title2",
                    url = "url2",
                    thumbnailUrl = "thumbnailUrl2"
                )
            )
        )

        val getPhotoListError = BaseResult.Error(
            message = "message",
            code = 500,
            errorBody = "errorBody"
        )
    }


}